extends KinematicBody2D

func _ready():
	speed = get_node("../../camera").camera_speed + 100;
	#speed = res://camera_scroll.gd;
	# Called every time the node is added to the scene.
	# Initialization here
	pass

export (int) var speed = 100

var target = Vector2()
var velocity = Vector2()
var distance = 0

func _input(event):
	pass
    #if event.is_action_pressed('click'):

func _physics_process(delta):
	target = get_global_mouse_position()
	velocity = (target - position).normalized() * (speed + speed)
	# rotation = velocity.angle()
	distance = (target - position).length()
	if (distance > 10):
		move_and_slide(velocity)
	else:
		move_and_slide(velocity * (distance / 10))
	if get_slide_count():
		get_node("../../camera").camera_speed = 0
	else:
		get_node("../../camera").camera_speed = 256
		
		
