extends Camera2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var camera_speed = 256

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	var pos = get_camera_position()
	pos.x += camera_speed * delta;
	self.position = pos
